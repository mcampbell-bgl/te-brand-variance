{% extends "layout-base.nunjucks" %}
{% set bodyclass="fixed-footer" %}

{% block content %}

<!--Page Header-->
    <header class="page-header">
        <div class="page-header__wrapper">
            <div class="page-header__section page-header__section--left">
                <img class="page-header__logo" src="images/brand-logo.png" alt="Brand Logo"/>
            </div>
            <div class="page-header__section page-header__section--center">
                
            <div class="breadcrumb">
                <div class="breadcrumb__section complete">
                    <div class="breadcrumb__line--base"></div>
                    <div class="breadcrumb__line"></div>
                    <div class="breadcrumb__disc"></div>
                    <p class="breadcrumb__text">Quote</p>
                </div>
                <div class="breadcrumb__section active partial">
                <div class="breadcrumb__line--base"></div>
                    <div class="breadcrumb__line"></div>
                    <div class="breadcrumb__disc"></div>
                    <p class="breadcrumb__text">Extras</p>
                </div>
                <div class="breadcrumb__section">
                    <div class="breadcrumb__line--base"></div>
                    <div class="breadcrumb__line"></div>
                    <div class="breadcrumb__disc"></div>
                    <p class="breadcrumb__text">Review</p>
                </div>
                <div class="breadcrumb__section">
                    <div class="breadcrumb__line--base"></div>
                    <div class="breadcrumb__line"></div>
                    <div class="breadcrumb__disc"></div>
                    <p class="breadcrumb__text">Pay</p>
                    <div class="breadcrumb__disc breadcrumb__disc--last"></div>
                    <p class="breadcrumb__text breadcrumb__text--last">Done</p>
                </div>
            </div>

            </div>
            <div class="page-header__section page-header__section--right">
                {% include "partials/generic/shopping-basket.nunjucks" %}
            </div>
        </div>

    </header>
    
<!--Page Header End-->


    <main>

        <div class="page-section--outer">
            <div class="page-section--inner">
                <div class="feature-header">
                    <h1 class="feature-header__heading">
                        <strong class="feature-header__heading-highlight-text">Keycare</strong>
                    </h1>
                    <p class="feature-header__sub-heading">
                        &pound;1000 of cover for your car, home and even your business premises keys.
                    </p>
                    <p class="feature-header__text">
                        Because you told us you needed cover for lost keys, we think you would be interested in Keycare. 
                    </p>
                    <div class="feature-header__divider"></div>
                    <p class="feature-header__price">
                        <strong class="feature-header__highlight-price-text">&pound;1.49/month</strong>
                        <span class="feature-header__standard-price-text"> (&pound;17.88/year)</span>
                    </p>
                </div>
            
                <div class="addon">
                    <p class="addon__text">
                        Access to a national network of locksmiths, cover for keys that have been permanently lost, stolen, or locked in the building or vehicle, new locks, and even reprogramming your immobiliser with no excess to pay.
                    </p>
                </div>
            </div>
        </div>

        <div class="page-section--outer">
            <div class="page-section--inner">
                <div class="fbels">
                    <div class="fbels__section fbels__section--full-width">
                        <h4 class="fbels__heading fbels__heading--cover">What it covers</h4>
                        <ul class="list list--fbels list--fbels-cover">
                            <li class="list__item">
                                Emergency helpline available 24 hours a day, 365 days per year
                            </li>
                            <li class="list__item">
                                Up to £1000 of cover per year for lost or stolen keys
                            </li>
                            <li class="list__item">
                                Keys for any lock are covered
                            </li>
                        </ul>
                    </div>
                    <div class="fbels__section fbels__section--full-width">
                        <h4 class="fbels__heading fbels__heading--remember">Things to think about</h4>
                        <ul class="list list--fbels list--fbels-remember">
                            <li class="list__item info">
                                Claims must be reported within 30 days
                            </li>
                            <li class="list__item info">
                                Claims relating to damaged keys or locks, or the result of wear and tear are not covered
                            </li>
                            <li class="list__item info">
                                Lost keys will usually require a period of three days before being replaced
                            </li>
                        </ul>
                    </div>
                </div>
                <p class="legal-text">
                    Administered by Keycare Ltd and underwritten by Ageas Insurance Ltd.
                </p>
                <a class="ipid-link" href="#0">Insurance Product Information Document</a>

                <div class="questions-box">
                    <div class="questions-box__divider"></div>
                    <p class="questions-box__text">Got a question on Keycare?</p>
                    <a href="#0" class="link questions-box__link">Ask a question and get an instant answer</a>
                </div>

            <div class="addon-radio">
                <p class="addon-radio__price">
                    <strong class="addon-radio__main-price">Add Keycare for &pound;1.49/month</strong>
                    <span class="addon-radio__sub-price">(&pound;17.88/year)</span>
                </p>
                <div class="form-row addon-radio__wrapper">
                    <fieldset class="form-row__field-set">
                        <legend class="form-row__legend--hidden">Would you like to add Keycare to your policy?</legend>
                        
                        <input class="form-row__input form-row__input--radio form-row__input--hidden" type="radio" name="grc" id="GRC-Yes" value="">
                        <label class="form-row__label form-row__label--pill addon-radio__radio" for="GRC-Yes">Yes</label>  
                                    
                        <input class="form-row__input form-row__input--radio form-row__input--hidden" type="radio" name="grc" id="GRC-No" value="">
                        <label class="form-row__label form-row__label--pill addon-radio__radio" for="GRC-No">No</label>                
                    </fieldset>
                </div>
            </div>
            </div>
        </div>


        <div class="page-section--outer">
            <div class="page-section--inner">
                <div class="button-container">
                    <a class="button button--primary button--large button--right" href="personal-accident.html"><strong>Next: </strong><span>Optional extra</span></a>
                    <a class="button button--text button--large button--back button--left" href="breakdown.html">Back</a>
                </div>
            </div>
        </div>







            
        


    </main>
    {% include "partials/generic/page-footer.nunjucks" %}



{% endblock %}