"use strict";

// PACKAGE(S) DEPENDENCIES
const browser           = require('browser-sync').create();
const del               = require('del');
const fs                = require('fs');
const gulp              = require('gulp');
const autoprefixer      = require('gulp-autoprefixer');
const babel             = require('gulp-babel');
const concat            = require('gulp-concat');
const data              = require('gulp-data');
const inject            = require('gulp-inject');
const nunjucks          = require('gulp-nunjucks-render');
const rename            = require('gulp-rename');
const sass              = require('gulp-sass');
const sequence          = require('gulp-sequence');
const sourcemaps        = require('gulp-sourcemaps');

const brands = [
    'autogeneral',
    'bbg',
    'bos',
    'budget',
    'coop',
    'dialdirect',
    'halifax',
    'legalgeneral',
    'lloyds',
    'mands',
    'o2',
    'postoffice',
    'rac',
    'rbs',
    'santander',
    'sunlife',
    'zenith'
];
const serve_base_url = 'zenith';

// PATH CONFIG.
const paths = {
    src: {
        common_css:         './content/src/common/sass/**/*.+(scss|sass)',
        common_scripts:     './content/src/common/scripts/**/*.js',
        brand_fonts:        './content/src/brand/[brandname]/fonts/**/*',
        brand_images:       './content/src/brand/[brandname]/images/**/*.+(png|svg|jpg|gif)',
        brand_sass:         './content/src/brand/[brandname]/sass/**/*.+(scss|sass)',
        components_sass:    './content/src/components/sass/**/*.+(scss|sass)',
        components_scripts: './content/src/components/scripts/**/*.js',
    },
    dest: {
        css:                './content/dist/[brandname]/css',
        fonts:              './content/dist/[brandname]/fonts',
        images:             './content/dist/[brandname]/images',
        root:               './content/dist/',
        scripts:            './content/dist/[brandname]/scripts',
    },
};

// PROCESS NUNJUCKS TEMPLATES - INC DATA (JSON)
gulp.task('nunjucks', function(){
    brands.map(function(brandname){
    
    gulp.src ('./nunjucks/pages/**/*.+(html|nunjucks)')
            .pipe(data(function(file){
                return JSON.parse(fs.readFileSync('./nunjucks/data/data.json'));
            }))
            .pipe(nunjucks({
                path: ['./nunjucks/templates']
            }))
            .pipe(gulp.dest('./content/dist/' + brandname ));
    });
    return;
});


// PROCESS SASS Inc. SOURCEMAPS + AUTOPREFIXER
gulp.task('sass', function(){    
    brands.map(function(brandname){
        return gulp.src(paths.src.brand_sass.replace('[brandname]', brandname))
            .pipe(sourcemaps.init())
            .pipe(inject(
                    gulp.src([paths.src.components_sass], { read: false, relative: false }), 
                    {
                        starttag: '/* inject:imports */',
                        endtag: '/* endinject */',
                        transform: function(filepath) {
                            return '@import ".' + filepath + '";';
                        }
                    }
                )
            )
            .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError ))
            .pipe(autoprefixer({ browsers: ['last 2 versions'], cascade: false }))
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest(paths.dest.css.replace('[brandname]', brandname)))
            .pipe(browser.stream());
        });
});


// CLEAN DIST DIR
gulp.task('clean', function(){
    // brands.map(function(brandname) {
    //     return del([paths.dest.root + brandname], { force: true });
    // });
    return del([paths.dest.root], { force: true });
});


// COPY IMAGE ASSETS
gulp.task('assets:images', function(){
    brands.map(function(brandname){
        return gulp.src([paths.src.brand_images.replace('[brandname]', brandname)])
        .pipe(gulp.dest(paths.dest.images.replace('[brandname]', brandname)));
    });
});


// COPY FONTS ASSETS
gulp.task('assets:fonts', function(){
    brands.map(function(brandname){
        return gulp.src([paths.src.brand_fonts.replace('[brandname]', brandname)])
        .pipe(gulp.dest(paths.dest.fonts.replace('[brandname]', brandname)));
    });
});


// COPY JAVASCRIPT ASSETS
gulp.task('assets:scripts', function(){
    brands.map(function(brandname){
        return gulp.src([ 
            paths.src.common_scripts, 
            paths.src.components_scripts 
        ])
        .pipe(gulp.dest(paths.dest.scripts.replace('[brandname]', brandname)));
    });
});


gulp.task('scripts', function () {
    return gulp.src('./Content/src/es6/*.js')
      .pipe(sourcemaps.init())
      .pipe(babel())
      .pipe(concat('bundle.js'))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./Content/dist/transpiledscripts'));
});



// GROUP ASSET PROCESSING
gulp.task('assets', sequence('clean', 'assets:images', 'assets:scripts', 'assets:fonts'));


// DEFAULT TASKS
gulp.task('default', sequence('assets', 'nunjucks', 'sass', 'serve'));


// DEV BROWSER
gulp.task('serve', function() {

    browser.init({
        server: {
            baseDir: paths.dest.root,
            directory: true,
        }
    });    

    // Watchers
    gulp.watch('./content/src/**/*.scss', ['sass'], browser.reload);
    gulp.watch('./content/src/nunjucks/**/*.nunjucks', ['nunjucks'], browser.reload);
    gulp.watch('./content/dist/**/*.html').on('change', browser.reload);

});







gulp.task('placeholders', function(){

    const imagefiles = [
        'icon-arrow-circle.svg',
        'icon-arrow-down.svg',
        'icon-arrow-right.svg',
        'icon-arrow-up.svg',
        'icon-audio.svg',
        'icon-calculator.svg',
        'icon-car.svg',
        'icon-chat.svg',
        'icon-comprehensive-cover.svg',
        'icon-cookie.svg',
        'icon-credit-card-black.svg',
        'icon-credit-card.svg',
        'icon-direct-debit-black.svg',
        'icon-direct-debit.svg',
        'icon-docs-online.svg',
        'icon-docs-post.svg',
        'icon-download.svg',
        'icon-edit.svg',
        'icon-envelope.svg',
        'icon-fbels-info.svg',
        'icon-file.svg',
        'icon-filter.svg',
        'icon-grc-lift.svg',
        'icon-grc-off.svg',
        'icon-grc-on.svg',
        'icon-home.svg',
        'icon-house.svg',
        'icon-houston-chat2.svg',
        'icon-houston-typing.svg',
        'icon-image.svg',
        'icon-info.svg',
        'icon-legal-protection.svg',
        'icon-like.svg',
        'icon-location.svg',
        'icon-logout.svg',
        'icon-minus-round-white.svg',
        'icon-minus.svg',
        'icon-monitor.svg',
        'icon-new-window.svg',
        'icon-passport.svg',
        'icon-personal-accident.svg',
        'icon-placeholder.svg',
        'icon-plus-round-white.svg',
        'icon-plus.svg',
        'icon-post.svg',
        'icon-print.svg',
        'icon-renew.svg',
        'icon-renewal.svg',
        'icon-search.svg',
        'icon-settings.svg',
        'icon-shopping-basket.svg',
        'icon-shopping-cart.svg',
        'icon-speech-bubbles.svg',
        'icon-telephone.svg',
        'icon-tick-square.svg',
        'icon-tick-white.svg',
        'icon-typing.svg',
        'icon-umbrella.svg',
        'icon-unlock.svg',
        'icon-visa-white.svg',
        'icon-visa.svg',
        'icon-wallet.svg',
        'logo-brand.svg',
        'logo-houston.svg',
        'logo-rac.svg',
        'brand-logo.svg',
        'icon-arrow-left.svg',
        'icon-bill.svg',
        'icon-close.svg',
        'icon-courtesy-car.svg',
        'icon-cross.svg',
        'icon-docs-eco.svg',
        'icon-document.svg',
        'icon-eu-travel.svg',
        'icon-fingerprint.svg',
        'icon-heart.svg',
        'icon-houston-help.svg',
        'icon-key.svg',
        'icon-lock.svg',
        'icon-minus-round.svg',
        'icon-no-hidden-fees.svg',
        'icon-phone.svg',
        'icon-plus-round.svg',
        'icon-printer.svg',
        'icon-right-arrow-square.svg',
        'icon-shopping-bag.svg',
        'icon-sms.svg',
        'icon-tick-circle.svg',
        'icon-tick.svg',
        'icon-upload.svg',
        'icon-windscreen.svg'
    ];

    imagefiles.map(function(filename){
        gulp.src('./content/src/brand/budget/images/icon-image.svg')
        .pipe(rename(filename))
        .pipe(gulp.dest('./icons/'));
    });
    return;
});