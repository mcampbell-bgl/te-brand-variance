(function () {
    var i;
    var j;
    var k;
    var l;
    var dateArray = [];
    var monthArray = [];
    var today = new Date();
    var todayYear = today.getFullYear();
    var todayMonth = today.getMonth();
    var todayDay = today.getDate();






    //create array of next 60 days and separate array of months from those days
    for (i = 0; i < 60; i++) {
        var day = new Date(todayYear, todayMonth, todayDay + i);
        dateArray.push(day);
        monthArray.push(day.getMonth());
    }

    //filter array to identify unique months
    monthArray = Array.from(new Set(monthArray));


    //filter array to get first date in each month
    var firstDayList = [];
    for (i = 0; i < monthArray.length; i++) {
        firstDayList.push(dateArray.find(function (e) {
            return e.getMonth() === monthArray[i];
        }));
    }

    //function that creates the date td block
    var dayBlock = function (thisDate, tabNo) {
        var dateBlock;
        var todaysDate = new Date();
        var todayClasses = "";


        if (todaysDate.setHours(0, 0, 0, 0) == thisDate.setHours(0, 0, 0, 0)) {
            todayClasses = 'today selected';
        } else {
            todayClasses = " ";
        }

        dateBlock = '<div class="calendar-table__body-segment calendar-table__body-segment--active ' + todayClasses + '" data-day="' + thisDate.getDay() + '" data-month="' + thisDate.getMonth() + '" data-year="' + thisDate.getFullYear() + '" data-panel="' + (tabNo + 1) + '"><a class="calendar-table__date-text" href="#0">' + thisDate.getDate() + '</a></div>';
        return dateBlock;
    };


    //function that creates an array of the active dates, grouped into months    
    var monthBlock = function () {
        var monthList = [];
        for (i = 0; i < monthArray.length; i++) {
            var subMonth = [];
            for (j = 0; j < dateArray.length; j++) {
                if ((dateArray[j].getMonth()) === monthArray[i]) {
                    subMonth.push(dayBlock(dateArray[j], i));
                }
            }
            monthList[i] = subMonth;
        }
        return monthList;
    };



    //switch for identifying indent for weekdays
    function getStartWeekday(num) {

        var indent;

        switch (num) {
            //sunday
            case 0:
                indent = 6;
                break;

            //monday
            case 1:
                indent = 0;
                break;

            //tuesday
            case 2:
                indent = 1;
                break;

            //Wednesday
            case 3:
                indent = 2;
                break;

            //thursday
            case 4:
                indent = 3;
                break;

            //friday
            case 5:
                indent = 4;
                break;

            //saturday
            case 6:
                indent = 5;
                break;

            default:
                indent = "Invalid day";
        }
        return indent;
    }



    //function to fill months with inactive days
    var fillMonth = function () {
        var initialList = monthBlock();
        var firstMonthDay;
        var monthTotalDays;
        for (i = 0; i < monthArray.length; i++) {

            firstMonthDay = firstDayList[i].getDate();
            if (firstMonthDay !== 1) {
                for (j = (firstMonthDay - 2); j >= 0; j--) {
                    initialList[i].unshift('<div class="calendar-table__body-segment"><p class="calendar-table__date-text">' + (j + 1) + '</p></div>');
                }
            }
            monthTotalDays = daysInMonth((firstDayList[i].getMonth() + 1), firstDayList[i].getFullYear());
            if (monthTotalDays !== initialList[i].length) {
                var daysToAdd = (monthTotalDays - (initialList[i].length));
                for (k = 0; k < daysToAdd; k++) {
                    initialList[i].push('<div class="calendar-table__body-segment"><p class="calendar-table__date-text">' + (initialList[i].length + 1) + '</p></div>');
                }
            }

            var firstDayMonth = new Date(firstDayList[i].getFullYear(), firstDayList[i].getMonth(), 1);
            var firstWeekday = getStartWeekday(firstDayMonth.getDay());


            for (l = 0; l < firstWeekday; l++) {
                initialList[i].unshift('<div class="calendar-table__body-segment calendar-table__body-segment--blank"><p class="calendar-table__date-text">0</p></div>');
            }

        }
        return initialList;
    };

    //translate dates array into html markup for tabs
    var tabBlock = function () {
        var dataBlock = fillMonth();
        var tabsCode = "";
        var blockCode = "";
        var checkedStatus;
        for (i = 0; i < dataBlock.length; i++) {
            if (i === 0) {
                checkedStatus = 'checked';
            } else {
                checkedStatus = " ";
            }
            var activeYear = firstDayList[i].getFullYear();
            var tabsCodeBlock = '<input class="tabs__input tabs__input--' + (i + 1) + ' calendar-tabs__input" id="Calendar-Tab-' + (i + 1) + '" type="radio" name="calendar-tabs" ' + checkedStatus + ' ><label class="tabs__label calendar-tabs__label" for="Calendar-Tab-' + (i + 1) + '" data-label-year="' + activeYear + '"><p class="tabs__label-text calendar-tabs__label-text">' + getMonthString(monthArray[i]) + '</p><div class="tabs__label-bar calendar-tabs__label-bar"></div></label>';
            tabsCode = tabsCode + tabsCodeBlock;

            blockCode = blockCode + '<section class="tabs__section tabs__section--' + (i + 1) + ' calendar-tab__section calendar-tab__section--' + (i + 1) + '"><div class="table__header-row"><div class="calendar-table__header-segment" data-day="1">Mon</div><div class="calendar-table__header-segment" data-day="2">Tues</div><div class="calendar-table__header-segment" data-day="3">Weds</div><div class="calendar-table__header-segment" data-day="4">Thurs</div><div class="calendar-table__header-segment" data-day="5">Fri</div><div class="calendar-table__header-segment" data-day="6">Sat</div><div class="calendar-table__header-segment" data-day="0">Sun</div></div>';
            for (j = 0; j < dataBlock[i].length; j++) {
                blockCode = blockCode + dataBlock[i][j];
            }
            blockCode = blockCode + '</section>';
        }

        tabsCode = tabsCode + blockCode;

        return tabsCode;
    };


    //SAMPLE CODE FOR TESTING
    var targetBlock = document.querySelector('.date-picker__tabs');
    targetBlock.innerHTML = tabBlock();


    //function for changing date display
    var dayElem = document.querySelector('.date-picker__selected-day');
    var monthElem = document.querySelector('.date-picker__selected-month');
    var dateInput = document.querySelector('#Start-Date-Input');

    var populateDate = function (clickedElem) {
        var dateDay = clickedElem.querySelector('.calendar-table__date-text').textContent;
        var dateMonth = getMonthString(parseInt(clickedElem.getAttribute('data-month')));
        var dateYear = clickedElem.getAttribute('data-year');

        var inputDate = dateYear + '-' + (twoDigit((parseInt(clickedElem.getAttribute('data-month')) + 1))) + '-' + twoDigit((parseInt(dateDay)));

        dayElem.innerHTML = dateDay;
        monthElem.innerHTML = (dateMonth + " " + dateYear);

        dateInput.value = inputDate;
    };

    // event listeners for functions


    //function for clearing day of the week in header
    var allWeekdays = document.querySelectorAll('.calendar-table__header-segment');

    var clearWeekdays = function () {
        for (i = 0; i < allWeekdays.length; i++) {
            classChange(allWeekdays[i], 'remove', 'active');
        }
    }

    //function to set day of week in header
    var setWeekday = function (targetElem) {
        var weekday = targetElem.getAttribute('data-day');
        var targetQuery = '.calendar-tab__section--' + targetElem.getAttribute('data-panel') + ' .calendar-table__header-segment[data-day="' + weekday + '"]';
        var targetWeekday = document.querySelector(targetQuery);
        classChange(targetWeekday, "add", "active");
    }

    //function for setting date
    var allTabInputs = document.querySelectorAll('.calendar-tabs__input');
    var todayDate = document.querySelector('.calendar-table__body-segment.today');
    var tomorrowDate = todayDate.nextSibling;

    var selectToday = document.querySelector('.date-picker__sub-button.today');
    var selectTomorrow = document.querySelector('.date-picker__sub-button.tomorrow');

    var resetTabs = function () {
        allTabInputs[i].checked = false;
        allTabInputs[0].checked = true;
    };

    var clearDateButtons = function () {
        classChange(selectToday, "remove", "active");
        classChange(selectTomorrow, "remove", "active");
    };


    var setDate = function (date) {
        for (i = 0; i < allTabInputs.length; i++) {
            resetTabs();
        }
        resetDatepicker();
        classChange(date, "add", "selected");
    };

    selectToday.addEventListener("click", function () {
        setDate(todayDate);
        populateDate(todayDate);
        clearDateButtons();
        classChange(selectToday, "add", "active");
        clearWeekdays();
        setWeekday(todayDate);
    });

    selectTomorrow.addEventListener("click", function () {
        setDate(tomorrowDate);
        populateDate(tomorrowDate);
        clearDateButtons();
        classChange(selectTomorrow, "add", "active");
        clearWeekdays();
        setWeekday(tomorrowDate);
    });

    var resetDatepicker = function () {
        for (i = 0; i < allActiveDates.length; i++) {
            classChange(allActiveDates[i], "remove", "selected");
        }
    };

    var allActiveDates = document.querySelectorAll('.calendar-table__body-segment--active');

    //set display to todays date on load
    populateDate(todayDate);
    setWeekday(todayDate);

    for (i = 0; i < allActiveDates.length; i++) {
        allActiveDates[i].addEventListener("click", function () {
            var self = this;
            resetDatepicker();
            classChange(self, "add", "selected");
            populateDate(self);
            clearDateButtons();
            if (self === todayDate) {
                classChange(selectToday, "add", "active");
            } else if (self == tomorrowDate) {
                classChange(selectTomorrow, "add", "active");
            }
            clearWeekdays();
            setWeekday(self);
        });
    }

    //function for changing year of calendar
    var calendarYear = document.querySelector('.date-picker__year-heading');

    var changeYear = function (targLabel) {
        calendarYear.innerHTML = targLabel.getAttribute("data-label-year");
    };

    var allTabs = document.querySelectorAll('.calendar-tabs__label');

    for (i = 0; i < allTabs.length; i++) {
        allTabs[i].addEventListener('click', function () {
            var self = this;
            changeYear(self);
        });
    }

})();