(function(){
    
    var validationBtn = document.querySelector("#Validation-Button");
    var formRows = document.querySelectorAll(".form-row");

    validationBtn.onclick = function(){
        for (i = 0; i < formRows.length; i++) {
            classChange(formRows[i], "add", "error");
        }
    };

})();