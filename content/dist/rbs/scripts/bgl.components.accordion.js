(function() {

    /*
    <div class='accordion'>
        <div class="accordion__section">
            <h5 class="accordion__heading-wrapper">
                <button class="accordion__heading-button" aria-expanded="false">
                    When will my cover start if I buy today?
                </button>
            </h5>
            <div class="accordion__body-wrapper">
                <p class="accordion__text">
                    At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati.
                </p>
            </div>
        </div>
    </div> 
    */


    
    var accordions = document.querySelectorAll('.accordion__section');

    for (var i = 0, max = accordions.length; i < max; i+1) {

        var accodion = accordions[i];
        var accordion_button = accordion.querySelector('.accordion__heading-button');
        var accordion_body = accordion.querySelector('.accordion__body-wrapper');

        // Hide accordions not explicitly marked to be open
        accordion_body.hidden = !accordion_body.classList.contains('start-open');

        // Add event handlers for buttons
        accordion_button.addEventListener('click', function(){
            toggle();
        });

    }


    // var accordionBodyList = document.querySelectorAll('.accordion__body-wrapper');

    // //close accordions on page load
    // for (i = 0; i < accordionBodyList.length; i++) {
    //     if(accordionBodyList[i].classList.contains("start-open") == false){
    //         accordionBodyList[i].hidden = 'true';
    //     }
        
    // }


    //function for toggling accordion
    var toggleAccordion = function(targElement){
        var targetBtn = targElement.querySelector('.accordion__heading-button');
        var targetHeading = targElement.nextElementSibling;

        targetBtn.onclick = function() {
            var expanded = targetBtn.getAttribute('aria-expanded') === 'true' || false;
            targetBtn.setAttribute('aria-expanded', !expanded);
            targetHeading.hidden = expanded;
        }
    }

    //loop to apply function to accordions
    var accordionHeadings = document.querySelectorAll('.accordion__heading-wrapper');
    for(j = 0; j < accordionHeadings.length; j++) {
        toggleAccordion(accordionHeadings[j]);
    }
    

})();


