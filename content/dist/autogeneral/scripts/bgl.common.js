var bgl = window.bgl || {};
    bgl.common = bgl.common || {};
    
bgl.common.pubsub = (function() {

    var events = {};

    function on (eventName, fn) {
        events[eventName] = events[eventName] || [];
        events[eventName].push(fn);
    }

    function off (eventName, fn) {
        if (events[eventName]) {
            for (var i = 0; i < events[eventName].length; i++) {
                if (events[eventName][i] === fn) {
                    events[eventName].splice(i, 1);
                    break;
                }
            }
        }
    }

    function emit (eventName, data) {
        if (events[eventName]) {
            events[eventName].forEach(function(fn) {
                fn(data);
            });
        }
    }

    function getEventCount(eventName) {
        return events[eventName].length;
    }


    function clearEvents () {
        events = {};
    }

    return {
        on: on,
        off: off,
        emit: emit,
        clear: clearEvents,
        getEventCount: getEventCount
    };


})();

bgl.common.loader = (function () {
    function load(componentConfiguration, componentUrl, containerElementId, callback) {
        $.ajax({
            url: componentUrl,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(componentConfiguration),
            success: function (data) {
                $('#' + containerElementId).html(data);
                if (typeof callback === "function") {
                    callback();
                }

                init(componentConfiguration);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    function init(componentConfiguration, callback) {
        bgl.common.errorModule.refreshComponentError(componentConfiguration);

        if (typeof callback === "function") {
            callback();
        }  
    }

    return {
        load: load,
        init: init
    };
})();

bgl.common.modal = (function () {
    
    // Public Functions
    function show(el, event) {
        el.addClass('show');
        el.attr('aria-hidden', false);
        $(document.body).addClass('modal-open');
        showBackdrop();
        event.stopPropagation();
    }

    function hide(el) {
        el.removeClass('show');
        el.attr('aria-hidden', true);
        $(document.body).removeClass('modal-open');
        $('.modal').off('click');
        removeBackdrop();
    }

    function showBackdrop() {
        var backdrop = document.createElement('div');
        backdrop.className = "modal-backdrop fade show";
        $(backdrop).appendTo(document.body);

        $('.modal').on('click', function (e) {
            if (e.target !== e.currentTarget) { 
                return; 
            }
            hide($('.modal'), e);
        });
    }

    function removeBackdrop() {
        $('.modal-backdrop').remove();
        $('.modal').off('click');
    }

    function loadModalContent(url, $popupContainer, popupConfiguration) {
        var requestData = "";
        
        if (popupConfiguration) {
            requestData = JSON.stringify(popupConfiguration);
        }

        $.ajax({
            url: url,
            type: 'POST',
            contentType: 'application/json',
            data: requestData,
            success: function (data) {
                $popupContainer.html(data);
            },
            error: function (error) {
                console.error(error);
            }
        });
    }

    // Event Handlers
    $(document).on('click', '[data-toggle="modal"]', function (event) {
        event.preventDefault();
        var url = $(this).data('url');
        var popupConfiguration = $(this).data('configuration');

        bgl.common.utilities.invoke($(this).data('togglePreModal'), $(this).data('togglePreModalParams'));

        var $relatedTarget = $($(this).data('target'));
        show($relatedTarget, event);

        if (url) {
            loadModalContent(url, $relatedTarget.find('.modal-body'), popupConfiguration);
        }
    });

    $(document).on('click', '[data-dismiss="modal"]', function (event) {
        event.preventDefault();
        var $relatedTarget = $(this).closest('.modal');
        hide($relatedTarget, event);
    });
})();

bgl.common.utilities = (function() {

    var globalIdentifier = this;

    function invoke(funcName, funcParams) {
        if (funcName !== undefined && funcName != null && funcName !== "") {
            var params = funcParams !== undefined && funcParams !== null ? funcParams.split(",") : [];

            var object = funcName.split(".");
            var fn = globalIdentifier;

            for (var i = 0, len = object.length; i < len && fn; i++) {
                fn = fn[object[i]];
            }

            if (typeof fn === "function") {
                fn.apply(null, params);
            } else {
                console.error(funcName + " is not a function.");
            }
        }
    }

    function setIdentifier(indentifier){
        if (indentifier !== undefined && indentifier !== null){
            globalIdentifier = indentifier;
        }
    }

    return {
        invoke: invoke,
        setIdentifier: setIdentifier
    };
})();

bgl.common.cookie = (function () {

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp("(?:^|; )" +
            name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));

        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function setCookie(name, value, options) {
        options = options || {};

        var expires = options.expires;

        if (typeof expires === "number" && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }

        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];

            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie + "; path=/";
    }

    function getUrlForCookieAsync(componentUrl) {
        var regex = /\/((\w+)|(\w+\/))$/gi;

        var url = componentUrl.replace(regex, "/UpdateCookie");

        return url;
    }

    function setCookieAsync(cookieKey, cookieValue, updateCookieUrl, onSuccess, onFailure) {

        $.ajax({
            url: updateCookieUrl,
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify({ "cookieKey": cookieKey, "cookieValue": cookieValue }),
            success: function (data) {
                if (data && data.isSuccess) {
                    if (typeof onSuccess === "function") {
                        onSuccess();
                    }
                }
            },
            error: function (error) {
                if (typeof onFailure === "function") {
                    onFailure();
                }
                console.log(error);
            }
        });
    }

    function deleteCookie(name) {
        deleteCookieByNameAndPath(name, "/");
    }

    function deleteCookieByNameAndPath(name, path) {
        setCookie(name, "", { expires: -1, path: path });
    }

    return {
        getCookie: getCookie,
        setCookie: setCookie,
        deleteCookie: deleteCookie,
        deleteCookieByNameAndPath: deleteCookieByNameAndPath,
        setCookieAsync: setCookieAsync,
        getUrlForCookieAsync: getUrlForCookieAsync
    };
})();

bgl.common.helptext = (function () {

    // Private Methods
    function positionHelpText(element) {

        // Temp cache main elements
        var $this = $(element);
        var $thisModal = $this.closest(".helptext__wrapper").find(".helptext");

        // Constants
        var MAX_MOBILE_BREAKPOINT = 480;
        var TOLERANCE_ADJUSTMENT_PX = 10;

        // Remove any previously set
        // - classes and properties
        $thisModal
            .removeClass('inverted')
            .removeClass('fullwidth')
            .removeClass('left')
            .removeClass('right')
            .css('display', '');

        // Window Properties
        var windowWidth = $(window).width();
        var windowInnerWidth = $(window).innerWidth();
        var windowInnerHeight = $(window).innerHeight();
        var windowScrollTop = $(window).scrollTop();

        // Help Icon Properties
        var elX = $this.offset().left;
        var elY = $this.offset().top;
        var elWidth = $this.outerWidth();

        // Calculate current position and available space
        var modalWidth = $thisModal.outerWidth() + TOLERANCE_ADJUSTMENT_PX;
        var modalHeight = $thisModal.outerHeight();
        var availableSpaceRHS = windowInnerWidth - (elX + elWidth);
        var availableSpaceLHS = elX;
        var remainingTop = elY - windowScrollTop;


        // Do we have minimum space required,
        // - if not likely mobile/sml device
        if (windowWidth < MAX_MOBILE_BREAKPOINT) {
            $thisModal.addClass('fullwidth');
        }
        else {
            // Determine optimal positioning and apply
            // - relevant classes Left|Right|FullWidth
            if (availableSpaceRHS < modalWidth) {
                if (!availableSpaceLHS < modalWidth) {
                    // Position modal on the left of the help icon
                    $thisModal.addClass('left');
                }
                else {
                    // Position modal on the left of the help icon
                    $thisModal.addClass('fullwidth');
                }
            }
            else {
                // Position modal on the right of the help icon
                $thisModal.addClass('right');
            }

            var elementOffset = elY + modalHeight;
            var windowOffset = windowInnerHeight + windowScrollTop;

            if (elementOffset > windowOffset && modalHeight < remainingTop) {
                // Apply inverted view
                $thisModal.addClass('inverted');
            }
        }
    }

    function showHelpText(element) {
        var $this = $(element);
        var $helpText = $this.closest(".helptext__wrapper").find(".helptext");

        positionHelpText($this);
        $helpText.attr('aria-hidden', false).fadeIn(300);
    }

    function hideHelpText() {
        $(".helptext").attr('aria-hidden', true).hide();
    }

    // Attach Events
    $(document).on('click', "[data-toggle='helptext']", function (event) {
        event.preventDefault();
        event.stopPropagation();
        hideHelpText();
        showHelpText(this);
    });

    $(document).on('click', "[data-dismiss='helptext']", function (event) {
        event.preventDefault();
        hideHelpText();
    });

    $(document).on('click', function (event) {
        var subject = $('.helptext__content');
        //console.log(subject.has(e.target).length);
        if (event.target.className !== subject.attr('class')
            && !subject.has(event.target).length) {
            hideHelpText();
        }
    });

    $(document).on('keyup', function (event) {
        if (event.keyCode === 27) {
            hideHelpText();
        }
    });

    $(window).on('resize', function (event) {
        hideHelpText();
    });

})();

bgl.common.datepicker = (function () {
    var defaultNumberOfMonths = 2;

    function init() {
        $("[data-role='datepicker']").each(function (i, item) {
            var configuration = initConfiguration($(item));

            setNumberOfMonthsVisible(configuration);

            $(item).datepicker(configuration);
        });
    }

    function initConfiguration($datepicker) {
        var configuration = $datepicker.data();
        var events = {
            onSelect: dateChanged
        };
        $.extend(configuration, events);

        return configuration;
    }

    function setNumberOfMonthsVisible(configuration) {
        if (configuration.numberOfMonths) {
            configuration.numberOfMonths = defaultNumberOfMonths;
        } else {
            $.extend(configuration, { numberOfMonths: defaultNumberOfMonths });
        }
    }

    function dateChanged(date, inst) {
        var datepicker = $(inst.input[0]);
        var hiddenFieldId = datepicker.data('dateFieldId');
        $('#' + hiddenFieldId).val(date).change();

        $('[data-dismiss="modal"]').click();
    }

    return {
        init: init
    };
})();

bgl.common.errorModule = (function() {
    var _this = {};
        
    _this.refreshComponentError = function refreshComponentError(componentConfiguration) {
        componentConfiguration = componentConfiguration || {};
        var componentId = componentConfiguration.Id;
        
        if (componentId) {
            var eventData = {
                componentId: componentId,
                isCriticalComponent: componentConfiguration.IsCritical,
                componentState: -1,
                componentErrorsCount: 0
            };

            var component = $('div[data-component-id="' + componentId + '"]');
        
            if (component.length) {
                eventData.componentState = component.data("componentState");
                
                if (eventData.componentState < 0) {
                    eventData.componentState = this.componentState.Error;
                }

                if (eventData.componentState !== this.componentState.Ok) {
                    eventData.componentErrorsCount = component.data("componentErrorsCount") || 0;

                    if (eventData.componentErrorsCount < 0) {
                        eventData.componentErrorsCount = 0;
                    }
                }

                bgl.common.pubsub.emit(this.componentErrorEventName, eventData);
            } 
        }
    }

    _this.componentState = {
        Ok: 0, 
        Error: 1, 
        Partial: 2
    };

    _this.componentErrorEventName = "ComponentErrorEvent";

    return _this;
})();
