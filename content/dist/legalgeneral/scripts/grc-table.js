(function(){

    var grcButtons = document.querySelectorAll('.grc__button');
    var allTiers = document.querySelectorAll('[data-grc]');
    //function for selecting grc tier

    var selectGrcTier = function(self) {

        var currentTier = self.getAttribute('data-grc');
        var queryFull = "[data-grc='" + currentTier + "']";
        var allCurrentTier = document.querySelectorAll(queryFull);
        var currentTierButton = document.querySelector(".grc__button" + queryFull);

        if(currentTierButton.classList.contains('active')) { 

            currentTierButton.innerHTML = 'Select';

            for (i=0; i < allCurrentTier.length; i++) {
                classChange(allCurrentTier[i], "remove", "active");
            }
        }
        else {
            for (i=0; i < grcButtons.length; i++) {
                grcButtons[i].innerHTML = 'Select';
            }

            currentTierButton.innerHTML = 'Selected';

            for (i=0; i < allTiers.length; i++) {
                classChange(allTiers[i], "remove", "active");
            }
            for (i=0; i < allCurrentTier.length; i++) {
                classChange(allCurrentTier[i], "add", "active");
            }
        }

        //prevents bubbling of event listener trigger
        event.stopImmediatePropagation();
    }



    for (i=0; i < allTiers.length; i++) {

        allTiers[i].onclick = function(){

            var thisElement = this;
            selectGrcTier(thisElement);

        }

    };



    setTimeout(function() {
        var grcRise = document.querySelector(".icon-grc__car-wrapper");
        grcRise.style.transform = "translateY(0%)";
    }, 200);
})()