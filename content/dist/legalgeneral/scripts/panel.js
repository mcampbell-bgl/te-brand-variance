(function() {


    // *************************
    // Model with Psuedo Code
    // *************************

    // 1. [Get elements]
    // 2. [Open Method]
    // 3. [Close Method]
    // 4. [Attached Event Handlers]

    // ** Notes on style
    // Watch the commas, variable names, variable delarations and whitespace e.g.

    // function declarations: function myFunction() {}
    // function expressions: var myfunction = function(){};

    // Always declare variables inc. for loops i.e. for(var i = 0; ....)
    // Consider optimisations


    // // Public Functions
    // function show(el, event) {
    //     el.addClass('show');
    //     el.attr('aria-hidden', false);
    //     event.stopPropagation();
    // }

    // function hide(el, event) {
    //     el.removeClass('show');
    //     el.attr('aria-hidden', true);
    //     $('.panel').off('click');
    // }

    // // Event Handlers
    // $(document).on('click', '[data-toggle="panel"]', function (event) {
    //     event.preventDefault();
    //     var $this = $(this);
    //     var $relatedTarget = $($this.data('target'));
    //     show($relatedTarget, event);
    // });

    // $(document).on('click', '[data-dismiss="panel"]', function (event) {
    //     event.preventDefault();
    //     var $relatedTarget = $(this).closest('.panel');
    //     hide($relatedTarget, event);
    // });


    //Slide Panel toggle active state function
    var toggleSlidePanel = function(slideId) {
        var targetSlide = document.querySelector(slideId);
        classChange(targetSlide, "toggle", "active");
        targetSlide.scrollTop = 1;
    };

    //function for triggering panels

    var triggerPanel = function(self){
        var targetPanelString = '#' + self.getAttribute('data-panel');
        toggleSlidePanel(targetPanelString);
        bodyNoScroll();
    }
    

   //Add event listeners to links that trigger panels (based on panel-link class)
    var panelLinks = document.querySelectorAll('.panel-link');
    for (i=0;i<panelLinks.length;i++) {

        panelLinks[i].onclick = function(){

            var thisElement = this;
            triggerPanel(thisElement);

        };
    };



    //Function for closing panel
    var closePanel = function(self){
        var targetParentPanelString = '#' + self.getAttribute('data-panel');
        toggleSlidePanel(targetParentPanelString);
        bodyNoScroll();
    };


    //event listeners for close links on panels
    var panelCloseLinks = document.querySelectorAll('.slide-panel__close');

    for (i=0;i<panelCloseLinks.length;i++) {

        panelCloseLinks[i].onclick = function(){
            var thisElement = this;
            closePanel(thisElement);
            
        };

        //keypress event listeners (space and enter keys)
        panelCloseLinks[i].onkeypress = function(e){
            if(e.keyCode == 13 || e.keyCode == 32){
                var thisElement = this;
                closePanel(thisElement);
            }
        };
    }


    // function for closing panel from header back button
    var closePanelFromHeader = function() {
        var allPagePanels = document.querySelectorAll('.slide-panel');
            for (var i = 0;  i < allPagePanels.length; i++) {
                classChange(allPagePanels[i], "remove", "active");
            }
            bodyNoScroll();
    };

    var headerBackBtns = document.querySelectorAll('.panel-header__back');

    for(var i = 0; i < headerBackBtns.length; i++) {

        headerBackBtns[i].onclick = function(){

            closePanelFromHeader();
        };

        //keypress event listeners (space and enter keys)
        headerBackBtns[i].onkeypress = function(e){
            if(e.keyCode == 13 || e.keyCode == 32){

                closePanelFromHeader();
            }
        };
    }


})();