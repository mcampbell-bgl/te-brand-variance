(function() {

    var excessEditBtn = document.querySelector("#Excess-Bar-Edit-Button");
    var excessEditBtnText = document.querySelector(".excess-bar__button-text");
    var excessEditModal = document.querySelector(".excess-bar__edit-modal");
    var excessEditBtnTextVal = excessEditBtnText.innerHTML;

    //function to change excess edit button text
    var changeExcessBtnText = function(){
        excessEditBtnTextVal = excessEditBtnText.innerHTML;

        if (excessEditBtnTextVal ==="Edit") {
            excessEditBtnText.innerHTML = "Cancel";
        }

        else {
            excessEditBtnText.innerHTML = "Edit";
        }
    }


    //Excess edit button on main panel event listeners

    
    excessEditBtn.onclick = function(){
        classChange(excessEditModal, "toggle", "active");
        changeExcessBtnText();
    }

    excessEditBtn.onkeypress = function(e){
        if(e.keyCode == 13 || e.keyCode == 28){
            classChange(excessEditModal, "toggle", "active");
            changeExcessBtnText();
        }
    }



    //excess options within modal
    var excessButtons = document.querySelectorAll(".excess-bar__modal-button");
    var volExcessDisplay = document.querySelector(".excess-bar__value--voluntary");
    var totalExcessDisplay = document.querySelector(".excess-bar__value--total");
    var excessBar = document.querySelector(".bar-graph__bar--two");


    //function for selecting excess option
    var selectExcess = function(self){
        for (j=0; j < excessButtons.length; j++){
            classChange(excessButtons[j], "remove", "selected");
        }

        classChange(self, "add", "selected");
        classChange(excessEditModal, "remove", "active");
        changeExcessBtnText();

        var selectedValue = parseInt((self.innerHTML.slice(1,4)), 10);

        volExcessDisplay.innerHTML = ('£' + selectedValue);
        totalExcessDisplay.innerHTML = ('£' + (selectedValue + 150));
        excessBar.style.height = ((((150 / (selectedValue + 150)) *100).toFixed(0)) + '%');
    }




    for (i=0; i < excessButtons.length; i++) {
        //Click events
        excessButtons[i].onclick = function(){
            var thisElement = this;
            selectExcess(thisElement);
        }

        //keypress events
        excessButtons[i].onkeypress = function(e){
            if(e.keyCode == 13 || e.keyCode == 32){

                var thisElement = this;
                selectExcess(thisElement);
            }
        }
    }
})()