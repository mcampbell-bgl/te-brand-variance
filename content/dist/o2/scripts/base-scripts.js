//Utilities (generic functions etc...)

//function to toggle class
var classChange = function(targetElem,type, className){
    if(type === "toggle") {
        targetElem.classList.toggle(className);
    }
    else if (type === "remove") {
        targetElem.classList.remove(className);
    }
    else if (type === "add"){
        targetElem.classList.add(className);
    }
}

//Toggle body no-scroll state function
var bodyNoScroll = function(){
    var bodyElement = document.querySelector('body');
    classChange(bodyElement, "toggle", "slide-open");
}