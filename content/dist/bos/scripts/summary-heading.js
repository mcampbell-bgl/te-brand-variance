(function() {
    var summaryHeadingButton = document.querySelector('.summary-heading__selector-option');
    var summaryHeadingModal = document.querySelector('.summary-heading__modal');
    var summaryHeadingModalOptions = document.querySelectorAll('.summary-heading__modal-option');

//click event for button
    summaryHeadingButton.onclick = function(){
        classChange(summaryHeadingModal, "toggle", "active");
    }

//enter keypress event for button
    summaryHeadingButton.onkeypress = function(e){
        if(e.keyCode == 13 || e.keyCode == 32){
            classChange(summaryHeadingModal, "toggle", "active");
        }
    }



//function for selecting modal option
var selectPaymentOption = function(self){
    for (j=0; j<summaryHeadingModalOptions.length;j++) {
        classChange(summaryHeadingModalOptions[j], "remove", "selected");
    }
    classChange(self, "add", "selected");

    classChange(summaryHeadingModal, "remove", "active");

    var summaryHeadingSchedule = self.getAttribute('data-pay-schedule');

    if (summaryHeadingSchedule === "monthly") {
        summaryHeadingButton.innerHTML = "a month";
    }
    else if (summaryHeadingSchedule === "annually") {
        summaryHeadingButton.innerHTML = "a year";
    }
}

//events for modal options
    for (i=0; i<summaryHeadingModalOptions.length;i++) {

        //click events
        summaryHeadingModalOptions[i].onclick = function(){
            var thisElement = this;
            selectPaymentOption(thisElement);
        }

        //keypress events
        summaryHeadingModalOptions[i].onkeypress = function(e){
            if(e.keyCode == 13 || e.keyCode == 28){
                var thisElement = this;
                selectPaymentOption(thisElement);
            }
        }
    }


    setTimeout(function() {
        var profileDisc = document.querySelector(".customer-profile__initials-wrapper");
        var profileInitials = document.querySelector(".customer-profile__initials");
        profileDisc.style.width = "120px";
        profileDisc.style.opacity = "1";
        profileInitials.style.opacity = "1";
    }, 50);
})()