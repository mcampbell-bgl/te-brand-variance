(function(){

    var breakdownButtons = document.querySelectorAll('.breakdown-table__button');
    var allTiers = document.querySelectorAll('[data-tier]');
    //function for selecting breakdown tier

    var selectBreakdownTier = function(self) {

        var currentTier = self.getAttribute('data-tier');
        var queryFull = "[data-tier='" + currentTier + "']";
        var allCurrentTier = document.querySelectorAll(queryFull);
        var currentTierButton = document.querySelector(".breakdown-table__button" + queryFull);

        if(currentTierButton.classList.contains('active')) { 

            currentTierButton.innerHTML = 'Select';

            for (i=0; i < allCurrentTier.length; i++) {
                classChange(allCurrentTier[i], "remove", "active");
            }
        }
        else {
            for (i=0; i < breakdownButtons.length; i++) {
                breakdownButtons[i].innerHTML = 'Select';
            }

            currentTierButton.innerHTML = 'Selected';

            for (i=0; i < allTiers.length; i++) {
                classChange(allTiers[i], "remove", "active");
            }
            for (i=0; i < allCurrentTier.length; i++) {
                classChange(allCurrentTier[i], "add", "active");
            }
        }

        //prevents bubbling of event listener trigger
        event.stopImmediatePropagation();
    }



    for (i=0; i < allTiers.length; i++) {

        allTiers[i].onclick = function(){

            var thisElement = this;
            selectBreakdownTier(thisElement);

        }

    };

    setTimeout(function() {
        var breakdownImage = document.querySelector(".breakdown-table__header-item--image");
        breakdownImage.style.backgroundPosition = "0px 0px";
        breakdownImage.style.backgroundSize = "100%";
    }, 50);
})()
