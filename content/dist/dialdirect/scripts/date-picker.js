(function() {

    var allSelectableDates = document.querySelectorAll('.calendar-table__body-segment--active');
    var selectedMonth = "";
    var selectedDay = "";
    var displayDateDay = document.querySelector(".date-picker__selected-day");
    var displayDateMonth = document.querySelector(".date-picker__selected-month");


    //function for selecting dates
    var selectDate = function(self){
        for (i=0;i < allSelectableDates.length; i++) {
            classChange(allSelectableDates[i], "remove", "selected");
        }

        selectedDay = self.querySelector(".calendar-table__date-text").innerHTML;
        selectedMonth = self.closest(".calendar-table").querySelector(".calendar-table__caption-text").innerHTML;

        classChange(self, "toggle", "selected");

        displayDateDay.innerHTML = selectedDay;
        displayDateMonth.innerHTML = selectedMonth;
    }


    for(i=0;i < allSelectableDates.length; i++) {

        var k = i;
        
        allSelectableDates[k].onclick = function() {
            
            var thisElement = this;
            selectDate(thisElement);

        };


        allSelectableDates[k].onkeypress = function(e) {
            if (e.keyCode == 13 || e.keyCode == 32) {

                var thisElement = this;
                selectDate(thisElement);
            }
        };
    }

})()