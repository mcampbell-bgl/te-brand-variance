(function(){

    var Datepicker = function(){

        // Constructor
            // Default settings i.e. daterange = 60
            // User settings - override defaults if neccessary

        var DATE_RANGE = 60;


        // Methods
            // utilities - filter, set date etc...
            
            // getDates
            // setDate
            // resetDate (setDate(today))
            // render datepicker control - (table 7col x 6rows, header for month and pagination)



            // from daterange determine months
            // getdays for each month
            // 

            var days   = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


            // DatePickerItem Object
            var DatePickerItem = {
                date: new Date(),
                day_name: '',
                day_index:  null,
                month_name: '',
                month_index: null,
                isInRange: true
            }

            var getDateRange = function () {

                var dateArray = [];
                var monthArray = [];
                var today = new Date();
                var todayYear = today.getFullYear();
                var todayMonth = today.getMonth();
                var todayDay = today.getDate();
                
                for (var i = 0, item; i < DATE_RANGE; i++) {

                    item = new DatePickerItem();

                    item.date = new Date(today, todayMonth, todayDay + i);
                    item.day_name  = days[item.date.getDay()];
                    item.day_index = item.date.getDay();
                    item.month_name = months[item.date.getMonth()];
                    item.month_index = item.date.getMonth();

                    dateArray.push(item);
                }

                return dateArray;

            };

            var getDaysInMonth = function (date) {
                return new Date(date.getYear(), date.getMonth()+1, 0).getDate();
            };

            var setDate = function () {
            };

            var resetDate = function () {
            };

            var renderDatePicker = function () {
            };


        // Attach events - wireup methods
            // onload
            // onclick


        // Return (api) exposing any applicable methods - only if needed.
        return {
            // Return any methods you want to expose
            // e.g. init: constructor,            
        };


    };

    //Datepicker.init();

})();